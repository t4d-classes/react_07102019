# Welcome to the React Class

## Instructor

Eric Greene

## Schedule

Class:

- Wednesday - Thursday, 9am to 5pm PDT

Breaks:

- Morning: 10:25am to 10:35am
- Lunch: Noon to 1pm
- Afternoon #1: 2:15pm to 2:25pm
- Afternoon #2: 3:40pm to 3:50pm

## Course Outline

- Day 1 - What is React, Functional Components, JSX, Props, Default Props, State, State Hook, Effect Hook
- Day 2 - Composition (including containment + specialization), Unit Testing, Other React Topics as Time Permits

### Requirements

- Node.js (version 10 or later)
- Web Browser
- Text Editor

### Instructor's Resources

- [DevelopIntelligence](http://www.developintelligence.com/)
