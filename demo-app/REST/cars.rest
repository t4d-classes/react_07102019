

GET http://localhost:3050/cars HTTP/1.1

###

POST http://localhost:3050/cars HTTP/1.1
Content-Type: application/json

{
  "make": "Ford",
  "model": "T",
  "year": 1920,
  "color": "black",
  "price": 400
}