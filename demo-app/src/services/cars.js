
const baseURL = 'http://localhost:3050/cars';

const getCollectionURL = () => {
  return baseURL;
};

const getElementURL = (elementId) => {
  return `${baseURL}/${encodeURIComponent(elementId)}`;
};

export const getAllCars = async () => {
  const res = await fetch(getCollectionURL());
  const cars = await res.json();
  
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(cars);
    }, 2000);
  });
};

export const createCar = async (car) => {
  const res = await fetch(getCollectionURL(), {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(car),
  });
  const savedCar = await res.json();
  return savedCar;
};

export const replaceCar = async (car) => {
  await fetch(getElementURL(car.id), {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(car),
  });
};

export const deleteCar = async (carId) => {
  await fetch(getElementURL(carId), {
    method: 'DELETE',
  });
};