import { useRef, useEffect } from 'react';

export const useDefaultInputFocus = () => {

  const defaultInput = useRef();

  useEffect(() => {

    if (defaultInput.current) {
      defaultInput.current.focus();
    }

  }, []);

  return defaultInput;
};

