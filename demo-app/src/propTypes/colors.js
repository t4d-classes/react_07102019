import PropTypes from 'prop-types';

export const colorsPropType = PropTypes.arrayOf(PropTypes.string);