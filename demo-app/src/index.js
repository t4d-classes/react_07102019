import React from 'react';
import ReactDOM from 'react-dom';

import { ColorTool } from './components/ColorTool';
import { CarTool } from './components/CarTool';

const colorList = [
  'red', 'blue', 'yellow', 'green', 'orange', 'black', 'indigo',
];

const carList = [
  { id: 1, make: 'Ford', model: 'Fusion Hybrid', year: 2018, color: 'silver', price: 25000 },
  { id: 2, make: 'Tesla', model: 'Model S', year: 2019, color: 'red', price: 100000 },
];


ReactDOM.render(
  <>
    {/* React.createElement(ColorTool, { colors: colorList }) */}
    {/* <ColorTool colors={colorList} headerText="Color Tool" /> */}
    <CarTool cars={carList} />
  </>,
  document.querySelector('#root'),
);


