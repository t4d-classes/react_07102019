import React, { useState, useCallback, useEffect } from 'react';

import { carsPropType } from '../propTypes/cars';
import { useDefaultInputFocus } from '../hooks/useDefaultInputFocus';
import {
  getAllCars,
  createCar as dbCreateCar,
  replaceCar as dbReplaceCar,
  deleteCar as dbDeleteCar,
 } from '../services/cars';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';

export const CarTool = () => {

  const [ cars, setCars ] = useState([]);
  const [ editCarId, setEditCarId ] = useState(-1);

  const defaultInput = useDefaultInputFocus();

  useEffect(() => {
    getAllCars().then(cars => setCars(cars));
  }, []);

  const init = useCallback(() => {
    setEditCarId(-1);
    if (defaultInput.current) {
      defaultInput.current.focus();
    }
  }, [ defaultInput ]);

  const addCar = useCallback((car) => {
    dbCreateCar(car)
      .then(getAllCars)
      .then(cars => setCars(cars))  
      .then(init);
  }, [ init ]);

  const replaceCar = useCallback(car => {
    dbReplaceCar(car)
      .then(getAllCars)
      .then(cars => setCars(cars))  
      .then(init);
  }, [ init ]);

  const deleteCar = useCallback((carId) => {
    dbDeleteCar(carId)
      .then(getAllCars)
      .then(cars => setCars(cars))  
      .then(init);
  }, [ init ]);

  const cancelCar = useCallback(() => {
    init();
  }, [ init ]);

  return <>
    <ToolHeader headerText="Car Tool" />
    <CarTable cars={cars} editCarId={editCarId}
      onEditCar={setEditCarId} onDeleteCar={deleteCar}
      onSaveCar={replaceCar} onCancelCar={cancelCar} />
    <CarForm buttonText="Add Car" onSubmitCar={addCar} ref={defaultInput} />
  </>;

};

CarTool.defaultProps = {
  cars: [],
};

CarTool.propTypes = {
  cars: carsPropType.isRequired,
};