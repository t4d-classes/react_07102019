import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { carPropType } from '../propTypes/cars';
import { useForm } from '../hooks/useForm';
import { useDefaultInputFocus } from '../hooks/useDefaultInputFocus';


export const EditCarRow = memo(({
  car,
  onSaveCar,
  onCancelCar: cancelCar,
}) => {

  const [ form, change ] = useForm({ ...car });

  const defaultInput = useDefaultInputFocus();

  const saveCar = () => {
    onSaveCar({
      ...form,
      id: car.id,
    });
  };

  return <tr>
    <td>{car.id}</td>
    <td><input type="text" name="make" ref={defaultInput}
      value={form.make} onChange={change} /></td>
    <td><input type="text" name="model" value={form.model} onChange={change} /></td>
    <td><input type="number" name="year" value={form.year} onChange={change} /></td>
    <td><input type="text" name="color" value={form.color} onChange={change} /></td>
    <td><input type="number" name="price" value={form.price} onChange={change} /></td>
    <td>
      <button type="button"
        onClick={saveCar}>Save</button>
      <button type="button"
        onClick={cancelCar}>Cancel</button>
    </td>
  </tr>
});

EditCarRow.propTypes = {
  car: carPropType,
  onSaveCar: PropTypes.func.isRequired,
  onCancelCar: PropTypes.func.isRequired,
};
