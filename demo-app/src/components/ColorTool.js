import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { colorsPropType } from '../propTypes/colors';

import { ToolHeader } from './ToolHeader';
import { ColorForm } from './ColorForm';

export const ColorTool = ({ colors: initialColors, headerText }) => {

  const [ colors, setColors ] = useState(initialColors.concat());

  const addColor = (colorObj) => {
    setColors(colors.concat(colorObj.color));
  };

  

  return <>
    <ToolHeader headerText="Color Tool" />
    <ul>
      {colors.map(color =>
        <li key={color}>
          {color}
      </li>)}
    </ul>
    <ColorForm buttonText="Add Color" onSubmitColor={addColor} />
  </>;

};

ColorTool.defaultProps = {
  colors: [],
};

ColorTool.propTypes = {
  headerText: PropTypes.string,
  colors: colorsPropType.isRequired,
};