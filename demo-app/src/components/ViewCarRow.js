import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { carPropType } from '../propTypes/cars';

export const ViewCarRow = memo(({
  car,
  onDeleteCar: deleteCar,
  onEditCar: editCar,
}) => {

  console.log('rendering view car row: ', car.id);

  // const deleteCar = () => {
  //   onDeleteCar(car.id);
  // }

  return <tr>
    <td>{car.id}</td>
    <td>{car.make}</td>
    <td>{car.model}</td>
    <td>{car.year}</td>
    <td>{car.color}</td>
    <td>{car.price}</td>
    <td>
      <button type="button"
        onClick={() => editCar(car.id)}>
          Edit
      </button>
      <button type="button"
        onClick={() => deleteCar(car.id)}>
          Delete
      </button>
    </td>
  </tr>;
});

ViewCarRow.propTypes = {
  car: carPropType,
  onDeleteCar: PropTypes.func.isRequired,
};