import React, { PureComponent, createRef } from 'react';
import PropTypes from 'prop-types';

import { carPropType } from '../propTypes/cars';

export class EditCarRow extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      ...props.car,
    };

    this.defaultInput = createRef();

    // this.change = this.change.bind(this);
    // this.saveCar = this.saveCar.bind(this);
  }

  change = (e) => {
    this.setState({
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value)
        : e.target.value,
    });
  }

  componentDidMount() {
    if (this.defaultInput.current) {
      this.defaultInput.current.focus();
    }
  }

  saveCar = () => {
    this.props.onSaveCar({
      ...this.state,
      id: this.props.car.id,
    });
  }

  render() {
    return <tr>
      <td>{this.props.car.id}</td>
      <td><input type="text" name="make" ref={this.defaultInput}
        value={this.state.make} onChange={this.change} /></td>
      <td><input type="text" name="model" value={this.state.model} onChange={this.change} /></td>
      <td><input type="number" name="year" value={this.state.year} onChange={this.change} /></td>
      <td><input type="text" name="color" value={this.state.color} onChange={this.change} /></td>
      <td><input type="number" name="price" value={this.state.price} onChange={this.change} /></td>
      <td>
        <button type="button"
          onClick={this.saveCar}>Save</button>
        <button type="button"
          onClick={this.props.onCancelCar}>Cancel</button>
      </td>
    </tr>;
  }
}

EditCarRow.propTypes = {
  car: carPropType,
  onSaveCar: PropTypes.func.isRequired,
  onCancelCar: PropTypes.func.isRequired,
};
