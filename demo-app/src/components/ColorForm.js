import React from 'react';
import PropTypes from 'prop-types';

import { useForm } from '../hooks/useForm';

export const ColorForm = ({ buttonText, onSubmitColor }) => {

  const [ colorForm, change, resetForm ] = useForm({
    color: '',
    hexCode: '',
  });

  const submitColor = () => {
    onSubmitColor({ ...colorForm });
    resetForm();
  };

  return <form>
    <div>
      <label htmlFor="color-input">Color:</label>
      <input type="text" id="color-input" name="color" value={colorForm.color} onChange={change} />
    </div>
    <div>
      <label htmlFor="hexcode-input">HexCode:</label>
      <input type="text" id="hexcode-input" name="hexCode" value={colorForm.hexCode} onChange={change} />
    </div>
    <button type="button" onClick={submitColor}>{buttonText}</button>
  </form>;
};

ColorForm.defaultProps = {
  buttonText: 'Submit Color',
};

ColorForm.propTypes = {
  buttonText: PropTypes.string,
  onSubmitColor: PropTypes.func.isRequired,
};