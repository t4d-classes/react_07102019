Exercise 1

https://gitlab.com/t4d-classes/react_07102019

1. Create a new component named Car Tool in the components folder.

2. In the component display the following HTML structure:

header
 |- h1
     |- Car Tool

3. Utilize the Car Tool component in index.js, and display the Car Tool underneath the Hello World component. Both components should be displayed at the same time. Use JSX to display both.

4. Ensure it works.