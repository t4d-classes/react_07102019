Exercise 5

https://gitlab.com/t4d-classes/react_07102019

1. Add a form to Car Tool. Create input fields for make, model, year, color and price.

2. Capture the data using a state hook.

3. Output the collected data to the console on each re-render

4. Ensure it works.